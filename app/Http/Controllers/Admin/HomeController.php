<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Visitor;
use App\Page;
use App\User;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $interval = intval($request->input('interval', 30));

        // Cotagem de visitantes
        if($interval > 120){
            $interval = 120;
        }
        if($interval < 30){
            $interval = 30;
        }
        $dateInterval = date('Y-m-d H:i:s', strtotime('-'.$interval.'days'));
        $visitsCount = Visitor::where('date_access', '>=', $dateInterval)->count();

        // Cotagem de usuário online
        $datelimit = date('Y-m-d H:i:s', strtotime('-5 minutes'));
        $onlineList = Visitor::select('ip')->where('date_access', '>=', $datelimit)->groupBy('ip')->get();
        $onlineCount = count($onlineList);

        // Cotagem páginas existentes no site
        $pageCount = Page::count();

        // Cotagem usuários criados no sistema
        $userCount = User::count();

        // Contagem para o PagePie
        $pagePie = [];
        $visitsAll = Visitor::selectRaw('page, count(page) as c')
                                    ->where('date_access', '>=', $dateInterval)
                                    ->groupBy('page')->get();
        foreach($visitsAll as $visit){
            $pagePie[$visit['page']] = intval($visit['c']);
        }

        $pageLabels = json_encode(array_keys($pagePie));
        $pageValues = json_encode(array_values($pagePie));
        $data = [
            "interval" => $interval,
            "visitsCount" => $visitsCount,
            "onlineCount" => $onlineCount,
            "pageCount" => $pageCount,
            "userCount" => $userCount,
            "pageLabels" => $pageLabels,
            "pageValues" => $pageValues
        ];

        return view('admin.home', $data);
    }
}
