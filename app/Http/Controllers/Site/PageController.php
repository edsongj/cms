<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Page;
use App\Visitor;

class PageController extends Controller
{
    public function index($slug)
    {
        $page = Page::where('slug', $slug)->first();
        if($page){
            $online = new Visitor;
            $online->ip = request()->ip();
            $online->date_access = date('Y-m-d H:i:s');
            $online->page = $page['slug'];
            $online->save();
            return view('site.page', [
                'page' => $page,
            ]);
        }else{
            abort(404);
        }
    }
}
