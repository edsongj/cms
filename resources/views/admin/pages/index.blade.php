@extends('adminlte::page')

@section('title', 'Páginas')
    
@section('content_header')
    <h1>
        Meus Usuários
        <a href="{{route('pages.create')}}" class="btn btn-sm btn-success">Nova Página</a>
    </h1>
    
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Título</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($pages as $page)
                        <tr>
                            <td style="width: 50px;">{{$page->id}}</td>
                            <td style="width: 700px">{{$page->title}}</td>
                            <td>
                                <a href="" target="_blank" class="btn btn-sm btn-primary">Visualizar</a>
                                <a href="{{route('pages.edit', ['page'=>$page->id])}}" class="btn btn-sm btn-info">Editar</a>

                                <form class="d-inline" action="{{route('pages.destroy', ['page'=>$page->id])}}" method="post" onsubmit="return confirm('Tem certeza que deseja excluir página {{$page->title}}?')">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-sm btn-danger">Excluir</button>
                                </form>

                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    {{$pages->links()}}

@endsection